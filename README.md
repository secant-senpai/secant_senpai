<!-- <body>
<h1 align="center">Secant_Senpai</h1>
<br>
<div align="center">
<img src ="https://media.giphy.com/media/44hlo3M3pqK9W/giphy.gif">
</div>
<br>
<div>
<img src="https://gitlab.com/secant-senpai/secant-senpai/-/raw/master/FineArt.jpg" align="right" height="300.0 px">
</div>
<div>
<b>I am a recent graduate with a Mechanical Engineering degree who somehow...<br>discovered that he is interested in programming and AI a little too late.</b><br><br>

<b>Feel free to look around, maybe you'll find some...<br>fine art around here?</b>
<br>
<h3 align="left">What Do I Know?</h3>
<li><b>Hardware: </b>CNC, 3D Printing, PLCs, Arduino, Raspberry Pi</li><br>
<li><b>Software: </b>Linux, AutoCAD, SOLIDWORKS, ANSYS, MATLAB / Simulink, FEA, CFD, Controls</li><br>
<li><b>Programming: </b>Git, Java / Spring, Python, C++, Visual Basic</li><br>
<li><b>Machine Learning: </b>Sci-kit Learn, Tensorflow / Keras, Eclipse Deeplearning4j, Computer Vision</li><br><br>

Warm Regards,<br>
Professional Rickroller.
</div>
</body> -->

![Penguins playing Computer (Linux)](https://gitlab.com/secant-senpai/secant-senpai/-/raw/master/LinuxTerminalCute.png "Wallpaper")

### What Do I Know?
**Hardware**: CNC, 3D Printing, PLCs, Arduino, Raspberry Pi  
**Software**: Linux, AutoCAD, SOLIDWORKS, ANSYS, MATLAB / Simulink, FEA, CFD, Controls  
**Programming**: Git, Java / Spring, Python, C++, Visual Basic  
**Machine Learning**: Sci-kit Learn, Tensorflow / Keras, Eclipse Deeplearning4j, Computer Vision

<p align="left">
	<a href="https://www.linkedin.com/in/chun-hong-har-184b2422a"><img src="https://gitlab.com/secant-senpai/secant-senpai/-/raw/master/linkedin.svg" alt="LinkedIn"></a>
</p>
